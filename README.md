# Function: for the same set of inputs, yield the same set of outputs.
# Get it right first!
# unfolding a function definition
# We can use calculations to prove properties about problems by using symbols
# substituting arguments into operation
# stepping back out of operation
# Proof by calculation is one way to connect problem specification to problem solution.
# How do you phrase programs as calculations?
# Don't hesitate returning to the first chapter later
# expressions: entities on which calculations are performed
# values: entities resulting from calculation; an expression on which no more calculation can be carried out
# note names == pitch classes
# atomic values (primitives)
# structured expressions e.g. list of pitches
# diverging expressions (bottom symbol); non-terminating calculation
# every expression has associated type
# types: sets of expressions which share much in common e.g. Integer, Char, PitchClass
# type signature: association of expression with its type
# :: == has type e.g. 42 :: Integer => 42 has type Integer
# Haskell programs are well-typed; strongly typed
# function types: T1 -> T2 => function maps values of type T1 to values of type T2 (domain = T1, range = T2)
# Haskell type system can infer correct types
# type signatures can serve as effective documentation
# normal use of function == function application
# infix functions often called operators
# infix = function written between two arguments
# infix operator as value => enclosed in parens
# function application >> precendence than operator application

ghci (Glasgow Haskell Compiler)

ghci basic.hs # loads file
main # executes file

Exercise 1.1: Write out all the steps in the calculation of the value of simple (simple 2 3 4) 5 6?

simple (simple 2 3 4) 5 6
=> (2 * (3 + 4)) * (5 + 6)
=> (2 * 7) * (11)
=> 14 * 11
=> 154

Exercise 1.2 Prove by calculation that simple (a - b) a b => a^2 - b^2

simple (a - b) a b 
=> (a - b) * (a + b)
=> a^2 + ab - ab - b^2
=> a^2 - b^2

Exercise 1.3 Identify the well-typed expressions in the following and give its proper type.

[ A, B, C ] => well typed, list of pitch class
[ D, 42 ] => poorly-typed
(-42, Ef) => well typed, (Integer, Pitch)

* Never give more than one binding for the same name in a context where the names can be confused.
* Functional abstraction: sequence of operations abstracted as a function.

1. note = pitch + duration
2. note :: Dur -> Pitch -> Music Pitch
3.  (:+:) :: Music Pitch -> Music Pitch -> Music Pitch
    m1 :+: m2 = music value represents playing m1 followed by m2
    (:=:) :: Music Pitch -> Music Pitch
    m1 :=: m2 is the music value that represents the playing of m1 and m2 simultaneously
4. trans :: Int -> Pitch -> Pitch
    trans i p is a pitch that is i semitones (half steps, or stepso on a piano) higher than p

function abstraction = generalization of naming
data structures <= motivated by data abstraction
Recursion is a powerful technique where larger problem browden down into several smaller but similar problems.
pattern matching
musical equality differs from Haskell value equivalence
This leads to an algebra of music
m :+: rest 0 === m
=== is musically equivalent to (rest of duration 0 + m === m)
polyphonic vs contrapuntal inerpretation (two indepdent lines or voices) -> sound the same, but different Haskell values
polymorphism
higher-order functions
modularity
numerical analysis: concerned with numerical incongriguities
Int (fits word size) vs Integer (very large)

Euterpea operators
* note,
* rest,
* (:+:),
* (:=:),
* trans 

Algebraic data types
Polymorphism: ability to parametrize, or abstract over types
Example: pitch with variable parameter
Type variables must begin with a lowercase letter to distinguish from concrete types
Type constructors take arguments, value constructors do not
polymorphic abstraction
type abstraction

data Music a =
    Prim (Primitive a)          -- primitive value
    | Music a :+: Music a       -- sequential composition
    | Music a :=: Music a       -- parallel composition
    | Modify Control (Music a)  -- modifier

fixity declaration

Music a defined in terms of Music a => inductive/recursive datatype
Control data type is used by Modify constructor to annotate a Music value with a tempo change, a transposition, a phrase attribute, an instrument, a key signature, or a custom label.

note, rest, tempo, transpose, instrument, phrase, keysig
Haskell layout rule: can have multiple expressions on same line separated by semi-
ii-V-I: chord progression

layout rule: first character of each equation in a let expression must line up vertically and subseuent lines must be ot the right of the first character

Start synthesizer
timidity -iA -Os

12-tone equal temperament scale
pentatonic blues scale consists of five notes (thus pentatonic) and in the key of C approximately corresponds to C, Eb, F, G, and Bb
- root, minor third, fourth, fifth, minor seventh

absolute pitch of a relative pitch can be defined mathematically as 12 times the octave plus the index of the pitch class
absolute pitch to pitch => more difficult for enharmonic equivalence, default to sharp
!! zero-based list indexing function => list !!n returns (n+1)th element in a list
transpose?

# Chapter 3: Polymorphic and Higher-order functions
Generic names for type => type variables
_ == wildcard pattern; matches any value and binds no variables
Exercises 2.1-2.5

In discerning the nature of a repeating pattern, it is sometimes helpful to identify the parts that are non-repeating. These will be the sources of parameterization. In the case above, these changing values are the functions absPitch and pitch.

map is an example of a higher-order function. To prove that new versions of functions are equivalent to the old ones can be done via calculation. This requires proof by induction.

mymap :: (a -> b) -> [a] -> [b]
map shows abstraction principle at work at type level.
Every expression in Haskell has a unique type, known as its principal type, the least general type that captures all valid uses of the expression.
Hindley-Milner type system forms basis of type systems of Haskell, ML, and several other languages. Assures existence of unique principal types.

whole tone scale: sequence of six ascending notes, with each adjacent pair of notes separated by two semitones, i.e a whole note.

transposition: refers to the process or operation of moving a collection of notes up or down in a pitch by a constant interval