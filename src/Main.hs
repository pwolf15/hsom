module Main where
import Euterpea
import HSoM

concertA, a440 :: (PitchClass, Octave)
concertA = (A, 4) -- concert A
a440     = (A, 4) -- A440

simple :: Integer -> Integer -> Integer -> Integer
simple x y z = x * (y + z)

main :: IO ()

pi :: Double
pi = 3.14159

hNote   :: Dur -> Pitch -> Int -> Music Pitch
hNote   d p i = note d p :=: note d (trans i p)

hList :: Dur -> [Pitch] -> Int -> Music Pitch
hList d [] i = rest 0
hList d (p : ps) i = hNote d p i :+: hList d ps i

main = print (simple 3 2 1)
