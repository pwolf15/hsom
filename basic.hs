module Main where
import Euterpea
import HSoM

-- Music
-- concertA, a440 :: (PitchClass, Octave)
-- concertA = (A, 4) -- concert A
a440     = (A, 4) -- A440

main :: IO ()

simple :: Integer -> Integer -> Integer -> Integer -- type signature
simple x y z = x * (y + z)

-- Functions
-- x :: Float
-- x = f (pi * r1 ** 2) + g (pi * r2 ** 2)

hNote :: Dur -> Pitch -> Music Pitch
hNote d p = note d p :=: note d (trans (-3) p)

hNote2 :: Dur -> Pitch -> Int -> Music Pitch
hNote2 d p i = note d p :=: note d (trans i p)

hList :: Dur -> [Pitch] -> Music Pitch
hList d [] = rest 0
hList d (p : ps) = hNote d p :+: hList d ps

hList2 :: Dur -> [Pitch] -> Int -> Music Pitch
hList2 d [] i = rest 0
hList2 d (p : ps) i = hNote2 d p i :+: hList2 d ps i

pc :: PitchClass
pc = C

-- ii-V-I progression
t251 :: Music Pitch
t251 = let dMinor = d 4 wn :=: f 4 wn :=: a 4 wn
           gMajor = g 4 wn :=: b 4 wn :=: d 5 wn
           cMajor = c 4 bn :=: e 4 bn :=: g 4 bn
        in dMinor :+: gMajor :+: cMajor

cVal :: Integer
cVal = let a = 1
                + 3
           b = 2
        in a + b

data BluesPitchClass = Ro | MT | Fo | Fi | MS   -- algebraic data type
type BluesPitch = (BluesPitchClass, Octave)     -- type synonym

ro, mt, fo, fi, ms :: Octave -> Dur -> Music BluesPitch
ro o d = note d (Ro, o);mt o d = note d (MT, o)
fo o d = note d (Fo, o);fi o d = note d (Fi, o)
ms o d = note d (MS, o)

-- fromBlues :: Music BluesPitch -> Music Pitch
-- fromBlues (Prim (Note d p)) = 
-- fromBlues (Prim (Rest d))   =
-- fromBlues (m1 :+: m2)       =
-- fromBlues (m1 :=: m2)       =
-- fromBlues (Modify )

myLength :: [a] -> Integer
myLength [] = 0
myLength (x:xs) = 1 + myLength xs

toAbsPitches :: [Pitch] -> [AbsPitch]
-- toAbsPitches [] = []
-- toAbsPitches (p:ps) = absPitch p : toAbsPitches ps

toPitches :: [AbsPitch] -> [Pitch]
-- toPitches [] = []
-- toPitches (a:as) = pitch a : toPitches as

mymap :: (a -> b) -> [a] -> [b]
mymap f [] = []
mymap f (x:xs) = f x : mymap f xs

toAbsPitches ps = mymap absPitch ps
toPitches as = mymap pitch as

wts :: Pitch -> [Music Pitch]
wts p = let f ap = note qn (pitch (absPitch p + ap))
        in map f [0, 2, 4, 6, 8]

-- turns a list of durations into a list of rests, 
--      each having the corresponding duration
-- f2 :: [Dur] -> [Music a]
f2 :: [Dur] -> [Music Pitch]
f2 ds = let f dur = rest dur
        in map f ds
-- main = print (f2 [1/4, 3/4, 2/4])

-- takes a list of music values, and
-- for each note, halves its duration and places a rest of that same duration after it
-- e.g. f3 [c 4 qn, d 4 en, e 4 hn] -> [c 4 en :+: rest en, d 4 sn :+: rest sn, e 4 qn :+: rest qn]
f3 :: [Music Pitch] -> [Music Pitch]
f3 ps = let f p = p
        in map f ps

main = print (f3 [c 4 qn, d 4 en, e 4 hn])
-- twoFiveOne :: Pitch -> Dur -> Music Pitch
-- twoFiveOne dur = let dMinor = d 4 dur :=: f 4 dur :=: a 4 dur
--                      gMajor = g 4 dur :=: b 4 dur :=: d 5 dur
--                      cMajor = c 4 dur * 2 :=: e 4 dur * 2 :=: g 4 dur * 2
--                  in dMinor :+: gMajor :+: cMajor

--  main = print (simple 30 9 5)
-- main = playDev 2 t251 
-- main = print (simple (simple 2 3 4) 5 6)
-- main = print (head [1, 2, 3])
-- main = print (myLength [1, 2, 3, 4])
-- main = print (tail [1, 2, 3])

-- f1 :: Int -> [Pitch] -> Pitch
-- f1 n p = let f p = trans (p + n)
--         in map f [A, B, C, D]

-- main = print (wts a440)
